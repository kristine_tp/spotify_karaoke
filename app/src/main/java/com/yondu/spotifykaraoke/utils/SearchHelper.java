package com.yondu.spotifykaraoke.utils;

import android.text.Html;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Kristine Peromingan on 5/5/2015.
 */
public class SearchHelper {

	public static final String SPOTIFY_SEARCH_PATH = "https://api.spotify.com/v1/search?q="; //"https://api.spotify.com/v1/search?q=br&type=track"
	public static final String SPOTIFY_INSTRUMENTAL = " instrumental";
	public static final String ITUNES_SEARCH_PATH = "https://itunes.apple.com/search?entity=song&term="; //"https://itunes.apple.com/search?term=torn&entity=song";

	private static String mSearchFilters;

	public static final String generateSearchUri(String searchString) {
		searchString = searchString.replace(" ", "+");
//		return SPOTIFY_SEARCH_PATH + searchString;
		return ITUNES_SEARCH_PATH + searchString;
	}

	public static final String generateArtistSearchUri(String artist) {
		String searchString = artist + "&type=artist";
		searchString = searchString.replace(" ", "+");
		return SPOTIFY_SEARCH_PATH + searchString;
	}

	public static final String generateInstrumentalSearchUri(String track, String artist) {
		String searchString = track + " " + artist + " instrumental" + "&type=track";
		searchString = searchString.replace(" ", "+");
		return SPOTIFY_SEARCH_PATH + searchString;
//		try {
//			searchString = URLEncoder.encode(searchString, "utf-8");
//		} catch(UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		return SPOTIFY_SEARCH_PATH + searchString;
	}


}
