package com.yondu.spotifykaraoke.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.Spotify;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.model.App;
import com.yondu.spotifykaraoke.model.User;
import com.yondu.spotifykaraoke.utils.DebugLog;
import com.yondu.spotifykaraoke.utils.SearchHelper;

/**
 * Created by Kristine Peromingan on 5/4/2015.
 */
public class SplashActivity extends Activity implements PlayerNotificationCallback, ConnectionStateCallback, View.OnClickListener {

	private static final int SPOTIFY_REQUEST_CODE = 1337;

	private Player mPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		findViewById(R.id.btn_login).setOnClickListener(this);
		spotifyLogin();
	}

	private void spotifyLogin() {
		AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(App.CLIENT_ID, AuthenticationResponse.Type.TOKEN, App.REDIRECT_URI);
		builder.setScopes(new String[] {"user-read-private", "streaming"});
		AuthenticationRequest request = builder.build();
		AuthenticationClient.openLoginActivity(this, SPOTIFY_REQUEST_CODE, request);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		if(requestCode == SPOTIFY_REQUEST_CODE) {
			AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
			if(response.getType() == AuthenticationResponse.Type.TOKEN) {
				User.login(response.getAccessToken());
				startActivity(new Intent(getApplicationContext(), SearchActivity.class));
				finish();
//				Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
//				mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
//					@Override
//					public void onInitialized(Player player) {
//						mPlayer.addConnectionStateCallback(SplashActivity.this);
//						mPlayer.addPlayerNotificationCallback(SplashActivity.this);
//						//mPlayer.play("spotify:track:2TpxZ7JUBn3uw46aR7qd6V");
//					}
//
//					@Override
//					public void onError(Throwable throwable) {
//						Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
//					}
//				});
			}
		}
	}

	@Override
	public void onLoggedIn() {
		DebugLog.d(this, "User logged in");
	}

	@Override
	public void onLoggedOut() {
		DebugLog.d(this, "User logged out");
	}

	@Override
	public void onLoginFailed(Throwable error) {
		DebugLog.d(this, "Login failed");
	}

	@Override
	public void onTemporaryError() {
		DebugLog.d(this, "Temporary error occurred");
	}

	@Override
	public void onConnectionMessage(String message) {
		DebugLog.d(this, "Received connection message: " + message);
	}

	@Override
	public void onPlaybackEvent(PlayerNotificationCallback.EventType eventType, PlayerState playerState) {
		Log.d("MainActivity", "Playback event received: " + eventType.name());
	}

	@Override
	public void onPlaybackError(PlayerNotificationCallback.ErrorType errorType, String errorDetails) {
		Log.d("MainActivity", "Playback error received: " + errorType.name());
	}

	@Override
	protected void onDestroy() {
		Spotify.destroyPlayer(this);
		super.onDestroy();
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_login) spotifyLogin();
	}
}
