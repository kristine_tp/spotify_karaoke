package com.yondu.spotifykaraoke.activities;

import android.app.Activity;
import android.os.Bundle;

import com.spotify.sdk.android.player.Spotify;
import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleActionBarActivity;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.fragments.RecordFragment;
import com.yondu.spotifykaraoke.fragments.SearchFragment;

/**
 * Created by Kristine Peromingan on 5/4/2015.
 */
public class SearchActivity extends SimpleActionBarActivity {

	public SearchActivity() {
		setInitialFragment(new SearchFragment());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		getBaseActionBar().setHomeAction(BaseActionBar.ActionHome.BACK, R.drawable.ic_back);
//		getBaseActionBar().setHomeAction(BaseActionBar.ActionHome.BACK, R.drawable.ic_back);
		setActionBarNavButtons(0, R.drawable.ic_back);
	}

//	@Override
//	public void onBackPressed() {
//		super.onBackPressed();
//	}

	@Override
	public void onDestroy() {
		// ALWAYS call this in your onDestroy() method, otherwise you will leak native resources!
		// This is an unfortunate necessity due to the different memory management models of
		// Java's garbage collector and C++ RAII.
		Spotify.destroyPlayer(this);
		super.onDestroy();
	}
}
