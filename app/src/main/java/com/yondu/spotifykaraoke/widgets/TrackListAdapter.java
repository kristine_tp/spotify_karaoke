package com.yondu.spotifykaraoke.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yondu.base.utils.FormatUtils;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.model.Track;

import java.util.List;

/**
 * Created by Kristine Peromingan on 5/11/2015.
 */
public class TrackListAdapter extends ArrayAdapter<Track> {

	private Context mContext;
	private int mLayoutResource, mSelectedPosition;

	public TrackListAdapter(Context context, int layoutResource, List<Track> tracks) {
		super(context, layoutResource, tracks);
		mContext = context;
		mLayoutResource = layoutResource;
	}

	public void setSelection(int position) {
		mSelectedPosition = position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderItem viewHolder;
		if(convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
			viewHolder = new ViewHolderItem();
			if(convertView.findViewById(R.id.txt_num) != null)
				viewHolder.numTxt = (TextView) convertView.findViewById(R.id.txt_num);
			viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
			viewHolder.artistTxt = (TextView) convertView.findViewById(R.id.txt_artist);
			convertView.setTag(viewHolder);
		} else viewHolder = (ViewHolderItem) convertView.getTag();

		Track mItem = getItem(position);
		if(viewHolder.numTxt != null) {
			viewHolder.numTxt.setText(FormatUtils.padZeros(position + 1, 2));
			viewHolder.numTxt.setSelected(position == mSelectedPosition);
		}
		viewHolder.titleTxt.setText(mItem.title);
		viewHolder.artistTxt.setText(mItem.artist);
		viewHolder.titleTxt.setSelected(position == mSelectedPosition);
		return convertView;
	}

	static class ViewHolderItem {
		TextView numTxt, titleTxt, artistTxt;
	}
}