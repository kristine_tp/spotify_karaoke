package com.yondu.spotifykaraoke.model;

/**
 * Created by Kristine Peromingan on 5/4/2015.
 */
public class User {

	private static User currUser;
	public String mSpotifyToken;

	public User(String spotifyToken) {
		mSpotifyToken = spotifyToken;
	}

	public static void login(String spotifyToken) {
		currUser = new User(spotifyToken);
	}

	public static User getCurrent() {
		return currUser;
	}
}
