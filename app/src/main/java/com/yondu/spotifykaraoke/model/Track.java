package com.yondu.spotifykaraoke.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 5/5/2015.
 */
public class Track {

	public String title, artist, trackUri, albumImgUri;
	private static List<Track> mSampleTracks;

	public Track(String title, String artist, String trackUri, String albumImgUri) {
		this.title = title;
		this.artist = artist;
		this.trackUri = trackUri;
		this.albumImgUri = albumImgUri;
	}

	public static List<Track> getSampleTracks() {
		if(mSampleTracks == null) {
			mSampleTracks = new ArrayList<Track>();
			mSampleTracks.add(new Track("I'm A Believer", "The Monkees", null, "https://i.scdn.co/image/06734360174db0db9590afaa4ed1875f767121b9"));
			mSampleTracks.add(new Track("Chandelier", "Sia", null, "https://i.scdn.co/image/fc213d7efcde95a9da0e9ed97cffd2fa7af90dec"));
			mSampleTracks.add(new Track("99 Red Balloons", "Nena", null, "http://is4.mzstatic.com/image/pf/us/r30/Music/8c/0e/f6/mzi.lzoxlnrn.300x300-75.jpg"));
			mSampleTracks.add(new Track("Smooth", "Santana, Rob Thomas", null, "https://i.scdn.co/image/bb5321d77ee1c2dc24c6cae8f12c9d337916940f"));
			mSampleTracks.add(new Track("Shake It Off", "Taylor Swift", null, "http://is1.mzstatic.com/image/pf/us/r30/Music3/v4/2c/b1/87/2cb187fa-2da2-839e-436a-8997561c0493/UMG_cvrart_00843930013562_01_RGB72_1400x1400_14UMDIM03405.300x300-75.jpg"));
			mSampleTracks.add(new Track("With or Without You", "U2", null, "http://is4.mzstatic.com/image/pf/us/r30/Features/64/ca/7a/dj.aibugkmk.300x300-75.jpg"));
			mSampleTracks.add(new Track("Californication", "Red Hot Chili Peppers", null, "https://i.scdn.co/image/a6acf5e95e997e07a877cf8bdfd8e7bae8d62c7d"));
			mSampleTracks.add(new Track("Bang Bang", "Jessie J, Ariana Grande, Nicki Minaj", null, "https://i.scdn.co/image/ae67edd58e83911d029e54458ae9d9f58522cbe0"));
			mSampleTracks.add(new Track("Happy Together", "The Turtles", null, "https://i.scdn.co/image/617f76330e4df2c232f006e12162d0569d8e919e"));
			mSampleTracks.add(new Track("No Woman No Cry", "Bob Marley & The Wailers", null, "https://i.scdn.co/image/6e8a71a1eb2ada5c1754c06e5a96c0a5f481db39"));
			mSampleTracks.add(new Track("Happy", "Pharrell Williams", null, "https://i.scdn.co/image/af369120f0b20099d6784ab31c88256113f10ffb"));
			mSampleTracks.add(new Track("Somebody That I Used To Know", "Gotye, Kimbra", null, "https://i.scdn.co/image/dae0acaf715d02ac483ee6de14506c3ccd3d00ee"));
			mSampleTracks.add(new Track("Come Together", "The Beatles", null, "http://is1.mzstatic.com/image/pf/us/r30/Music/17/07/36/mzi.ldnorbao.300x300-75.jpg"));
			mSampleTracks.add(new Track("I Don't Want to Miss a Thing", "Aerosmith", null, "https://i.scdn.co/image/eee5f6672a3523af9fe8c350996985511645e3c8"));
			mSampleTracks.add(new Track("Build Me Up Buttercup", "The Foundations", null, "https://i.scdn.co/image/0e606a9b964b2f1762648c78a2973283dfdb24ee"));
			mSampleTracks.add(new Track("Girls Just Wanna Have Fun", "Cyndi Lauper", null, "https://i.scdn.co/image/7955afad3010b5a75b9674a6acd4f7aa63e91b1f"));
			mSampleTracks.add(new Track("Gangnam Style (강남스타일)", "PSY", null, "https://i.scdn.co/image/c2cc04307dfdcfc78efa4a38ab10aba269abae40"));
			mSampleTracks.add(new Track("Budapest", "George Ezra", null, "http://is1.mzstatic.com/image/pf/us/r30/Music1/v4/94/19/cb/9419cb72-7e43-0462-91b8-5656b5702f3c/886445001372.300x300-75.jpg"));
			mSampleTracks.add(new Track("A Thousand Years", "Christina Perri", null, "https://i.scdn.co/image/c8bf6fa58ad47ed39b8c741a5bc9fb3a9d52449a"));
			mSampleTracks.add(new Track("Geronimo", "Sheppard", null, "https://i.scdn.co/image/32031982517529900c02654c460dc9ac6c47c598"));
			mSampleTracks.add(new Track("Que sera sera", "Doris Day", null, "https://i.scdn.co/image/4e852f936227a0ba3dc1d7ff3441ed96964842e4"));
			mSampleTracks.add(new Track("Moves Like Jagger (feat. Christina Aguilera)", "Maroon 5", null, "http://is4.mzstatic.com/image/pf/us/r30/Music3/v4/e3/61/25/e361251b-3ca7-8878-fc9c-3b53c8f674dc/UMG_cvrart_00602537943937_01_RGB72_1406x1406_14UMGIM27067.300x300-75.jpg"));
			mSampleTracks.add(new Track("Sugar, Sugar", "The Archies", null, "https://i.scdn.co/image/cdfab20eaba8f34484cbb1f7a53bbf69405eb355"));
			mSampleTracks.add(new Track("The Phoenix", "Fall Out Boy", null, "https://i.scdn.co/image/950b1d631d47daa2caf36efe450ee0ec78e018f3"));
		}
		return mSampleTracks;
	}
}
