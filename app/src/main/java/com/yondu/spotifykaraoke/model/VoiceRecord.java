package com.yondu.spotifykaraoke.model;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;

import com.yondu.spotifykaraoke.utils.DebugLog;

import java.io.IOException;

/**
 * Created by Kristine Peromingan on 5/15/2015.
 */
public class VoiceRecord {

	private String mFileName;

	private boolean mIsRecording;
	private boolean mIsPlaying;
	private MediaRecorder mRecorder;
	private MediaPlayer mPlayer;

	public VoiceRecord() {
		mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
		mFileName += "/audiorecordtest.3gp";
	}

	public void release() {
		if(mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}

		if(mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	public void toggleRecord() {
		mIsRecording = !mIsRecording;
		DebugLog.d(this, "record " + mIsRecording);
		if(mIsRecording) startRecording();
		else stopRecording();
	}

	public void togglePlay() {
		mIsPlaying = !mIsPlaying;
		DebugLog.d(this, "play " + mIsPlaying);
		if(mIsPlaying) startPlaying();
		else stopPlaying();
	}

	private void startPlaying() {
		mPlayer = new MediaPlayer();
		try {
			mPlayer.setDataSource(mFileName);
			mPlayer.prepare();
			mPlayer.start();
		} catch(IOException e) {
			DebugLog.eStackTrace(e);
		}
	}

	private void stopPlaying() {
		if(mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	private void startRecording() {
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setOutputFile(mFileName);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

		try {
			mRecorder.prepare();
		} catch(IOException e) {
			DebugLog.eStackTrace(e);
		}
		mRecorder.start();
	}

	private void stopRecording() {
		if(mRecorder != null) {
			mRecorder.stop();
			mRecorder.release();
			mRecorder = null;
		}
	}
}
