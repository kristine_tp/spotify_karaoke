package com.yondu.spotifykaraoke.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SlidingPaneLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.BaseFragmentActivity;
import com.yondu.base.utils.FormatUtils;
import com.yondu.base.utils.SingleHttpRequestTask;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.model.App;
import com.yondu.spotifykaraoke.model.Track;
import com.yondu.spotifykaraoke.model.User;
import com.yondu.spotifykaraoke.utils.DebugLog;
import com.yondu.spotifykaraoke.utils.SearchHelper;
import com.yondu.spotifykaraoke.widgets.TrackListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 5/4/2015.
 */
@SuppressLint("ValidFragment")
public class TrackFragment extends BaseActionBarFragment implements ConnectionStateCallback, PlayerNotificationCallback, View.OnClickListener, AdapterView.OnItemClickListener, View.OnTouchListener, SeekBar.OnSeekBarChangeListener, SingleHttpRequestTask.OnHttpRequestListener {

	private static final String LYRICS_SEARCH_PATH = "http://lyrics.wikia.com/api.php?artist=";
	private static final String LYRICS_SEARCH_SONG = "&song=";
	private static final int REQUEST_SEARCH_INSTRUMENTAL = 101;
	private static final int REQUEST_SEARCH_LYRICS = 102;
	private static final int REQUEST_SEARCH_LYRICS_2 = 103;
	private static final int REQUEST_GET_LYRICS = 104;

	private int mCurrInstrumentalTrack;
	private boolean isInitialized, isActive, isPlaying, isSeeking;
	private String mLyrics;
	private View mMainView, mLoadTrack, mLoadLyrics, mLoadSearchListTracks;
	private TextView mTimeTxt, mLyricsTxt, mNoLyricsTxt, mSearchListTracksTxt;
	private ImageView mPlayToggleBtn;
	private SeekBar mTrackSeek;
	private ListView mTrackListView;
	private SlidingPaneLayout mSlider;
	private WeakReference<Activity> mWeakActivity;
	private Track mTrack;
	private List<Track> mInstrumentalTracks;
	private TrackListAdapter mTracksAdapter;
	private Player mPlayer;
	private PlayerState mPrevPlayerState;
	private SingleHttpRequestTask mInstrumentalSearchRequest;
	private Thread mTrackPositionThread;
	private Handler mTrackPositionThreadHandler = new Handler();

	public TrackFragment(Track track) {
		mTrack = track;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.fragment_track, container, false);
		mWeakActivity = new WeakReference<Activity>(getActivity());
		initView();
		searchLyrics(false);
		return mMainView;
	}

	@Override
	public void onResume() {
		super.onResume();
		if(mPlayer != null) {
			mPlayer.addPlayerNotificationCallback(this);
			mPlayer.addConnectionStateCallback(this);
		}
		resumeTrackPositionThread();
	}

	@Override
	public void onPause() {
		if(mPlayer != null) {
			mPlayer.removePlayerNotificationCallback(this);
			mPlayer.removeConnectionStateCallback(this);
		}
		pauseTrackPositionThread();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		stopTrackPositionThread();
		((BaseFragmentActivity) getActivity()).unregisterLifecycleObserver(mActivityLifecycleObserver);
//		if(mPlayer != null) try {
//			mPlayer.pause();
//		} catch(RejectedExecutionException e) {
//			DebugLog.e(this, e.toString());
//			Spotify.destroyPlayer(getActivity());
//		}
		Spotify.destroyPlayer(this);
		super.onDestroy();
	}

	private void initView() {
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getBaseActionBar().setTitleText("Now Playing");
		((BaseFragmentActivity) getActivity()).registerLifecycleObserver(mActivityLifecycleObserver);
		DebugLog.d(this, "\"" + mTrack.title + "\", \"" + mTrack.artist + "\", null, \"" + mTrack.albumImgUri + "\"");

		mTimeTxt = (TextView) mMainView.findViewById(R.id.txt_time);
		mLyricsTxt = (TextView) mMainView.findViewById(R.id.txt_lyrics);
		mNoLyricsTxt = (TextView) mMainView.findViewById(R.id.no_lyrics);
		mSearchListTracksTxt = (TextView) mMainView.findViewById(R.id.txt_search_instrumental_list);
		mLoadTrack = mMainView.findViewById(R.id.load_track);
		mLoadLyrics = mMainView.findViewById(R.id.load_lyrics);
		mLoadSearchListTracks = mMainView.findViewById(R.id.load_search_list_tracks);
		mTrackListView = (ListView) mMainView.findViewById(R.id.list_instrumentals);

		mPlayToggleBtn = (ImageView) mMainView.findViewById(R.id.btn_play_toggle);
		mPlayToggleBtn.setOnClickListener(this);
		mMainView.findViewById(R.id.lyrics).setOnTouchListener(this);

		mTrackSeek = (SeekBar) mMainView.findViewById(R.id.seek_track);
		mTrackSeek.setOnSeekBarChangeListener(this);
		mTrackSeek.setEnabled(false);

		mSlider = (SlidingPaneLayout) mMainView.findViewById(R.id.slider);
		mSlider.openPane();
		mSlider.setSliderFadeColor(Color.TRANSPARENT);

		((TextView) mMainView.findViewById(R.id.txt_title)).setText(mTrack.title);
		((TextView) mMainView.findViewById(R.id.txt_lyrics_title)).setText(mTrack.title);
		((TextView) mMainView.findViewById(R.id.txt_lyrics_artist)).setText(mTrack.artist);
	}

	private void searchInstrumentalTrack() {
		if(mInstrumentalSearchRequest == null || !mInstrumentalSearchRequest.isActive()) {
			((TextView) mMainView.findViewById(R.id.txt_artist)).setText("Searching for instrumental version");
			mSearchListTracksTxt.setText("Searching for instrumental tracks");
			mLoadTrack.setVisibility(View.VISIBLE);
			mLoadSearchListTracks.setVisibility(View.VISIBLE);

			String artist = mTrack.artist;
			artist = !artist.contains(",") ? artist : artist.substring(0, artist.indexOf(","));
			artist = !artist.contains("&") ? artist : artist.substring(0, artist.indexOf("&"));
//			String title = !mTrack.title.contains("(") ? mTrack.title : mTrack.title.substring(0, mTrack.title.indexOf("(")-1);
			String title = mTrack.title;
			String searchString = SearchHelper.generateInstrumentalSearchUri(title, artist);
			mInstrumentalSearchRequest = new SingleHttpRequestTask(REQUEST_SEARCH_INSTRUMENTAL, mWeakActivity, this);
			mInstrumentalSearchRequest.execute(new SingleHttpRequestTask.RequestParams(searchString));
//		searchRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(searchString));
			DebugLog.i(this, "instrumental: " + searchString);
		}
	}

	private void searchLyrics(boolean isSearchAgain) {
		mLoadLyrics.setVisibility(View.VISIBLE);
		mNoLyricsTxt.setVisibility(View.GONE);
		mLyricsTxt.setVisibility(View.GONE);

		String searchString = generateSearchString(isSearchAgain);
		SingleHttpRequestTask searchRequest = new SingleHttpRequestTask(isSearchAgain ? REQUEST_SEARCH_LYRICS_2 : REQUEST_SEARCH_LYRICS, mWeakActivity, this);
		searchRequest.execute(new SingleHttpRequestTask.RequestParams(searchString));
//		searchRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(searchString));
		DebugLog.i(this, "lyrics: " + searchString);
	}

	private String generateSearchString(boolean cleanString) {
		String artist = mTrack.artist;
		String searchString = !artist.contains(",") ? artist : artist.substring(0, artist.indexOf(","));
		searchString = !searchString.contains("&") ? searchString : searchString.substring(0, searchString.indexOf("&"));

		String title = cleanString ? mTrack.title.replace("&", "and") : mTrack.title;
		searchString += LYRICS_SEARCH_SONG + title;
		searchString = searchString.replace(" ", "%20");
		return LYRICS_SEARCH_PATH + searchString;
	}

	private void playTrack() {
		if(mInstrumentalTracks != null && mInstrumentalTracks.size() > 0)
			playTrack(mInstrumentalTracks.get(mCurrInstrumentalTrack));
		else searchInstrumentalTrack();
	}

	private void playTrack(final Track track) {
		((TextView) mMainView.findViewById(R.id.txt_title)).setText(track.title);
		((TextView) mMainView.findViewById(R.id.txt_artist)).setText(track.artist);

		mPrevPlayerState = null;
		mPlayToggleBtn.setImageResource(R.drawable.ic_play);
		mLoadTrack.setVisibility(View.VISIBLE);
		mTimeTxt.setVisibility(View.GONE);
		mTrackSeek.setEnabled(false);
		mTrackSeek.setProgress(0);
		stopTrackPositionThread();

		if(mPlayer == null || !mPlayer.isInitialized()) {
			try {
				DebugLog.d(this, getActivity() + " " + User.getCurrent());
				DebugLog.d(this, User.getCurrent().mSpotifyToken);
				Config playerConfig = new Config(getActivity().getApplicationContext(), User.getCurrent().mSpotifyToken, App.CLIENT_ID);

				Player.Builder builder = new Player.Builder(playerConfig);
				builder.setCallbackHandler(mPlayerHandler);
				mPlayer = Spotify.getPlayer(builder, this, new Player.InitializationObserver() {
					@Override
					public void onInitialized(Player player) {
						mPlayer = player;
						DebugLog.d(this, "player: " + player);
						mPlayer.addConnectionStateCallback(TrackFragment.this);
						mPlayer.addPlayerNotificationCallback(TrackFragment.this);
						mPlayer.play(track.trackUri);
						DebugLog.d(this, "play " + track.trackUri);
					}

					@Override
					public void onError(Throwable throwable) {
						DebugLog.e(this, "error: " + throwable.getMessage());
					}
				});
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else
			mPlayer.play(track.trackUri);
	}

	private void getLyrics(String lyricsUri) {
		SingleHttpRequestTask searchRequest = new SingleHttpRequestTask(REQUEST_GET_LYRICS, mWeakActivity, this);
		searchRequest.execute(new SingleHttpRequestTask.RequestParams(lyricsUri));
//		searchRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(lyricsUri));
		DebugLog.w(this, lyricsUri);
	}

	private void onSearchInstrumentalTrack(boolean isSuccess, String response) {
		if(isSuccess && response.length() > 0) {
			try {
				JSONObject tracksJson = new JSONObject(response).getJSONObject("tracks");
				if(tracksJson.getInt("total") > 0) {
					mInstrumentalTracks = new ArrayList<Track>();
					JSONArray tracks = tracksJson.getJSONArray("items");
					for(int i = 0; i < tracks.length(); i++) {
						JSONObject track = tracks.getJSONObject(i);
						String artists = "";
						JSONArray artistsJson = track.getJSONArray("artists");
						for(int j = 0; j < artistsJson.length(); j++)
							artists += (artists.length() > 0 ? ", " : "") + artistsJson.getJSONObject(j).getString("name");
						mInstrumentalTracks.add(new Track(track.getString("name"), artists, track.getString("uri"), null));
					}

					playTrack(mInstrumentalTracks.get(0));
					setTrackListView();
					return;
				}
			} catch(JSONException e) {
				e.printStackTrace();
			}
		}

		mLoadSearchListTracks.setVisibility(View.GONE);
		mSearchListTracksTxt.setText(getResponseErrorMsg(response, "No instrumental tracks found"));
		((TextView) mMainView.findViewById(R.id.txt_artist)).setText(getResponseErrorMsg(response, "No instrumental version found"));
		mLoadTrack.setVisibility(View.GONE);
	}

	private void setTrackListView() {
		try {
			mTracksAdapter = new TrackListAdapter(getContext(), R.layout.view_instrumental_item, mInstrumentalTracks);
			mTrackListView.setAdapter(mTracksAdapter);
			mTrackListView.setVisibility(View.VISIBLE);
			mTrackListView.setOnItemClickListener(this);
		} catch(Exception e) {
			DebugLog.eStackTrace(e);
		}
	}

	private void onSearchLyrics(int searchRequestCode, boolean isSuccess, String response) {
		if(isSuccess && response.length() > 0) {
			if(response.indexOf("<pre>\nNot found</pre>") == -1) {
				try {
					String info = response.substring(response.indexOf("Additional Info:"));
					String link = info.substring(info.indexOf("http"));
					link = link.substring(0, link.indexOf('"'));
					getLyrics(link);
					return;
				} catch(StringIndexOutOfBoundsException e) {
					DebugLog.w(this, e.getMessage());
					e.printStackTrace();
				}
			} else if(searchRequestCode == REQUEST_SEARCH_LYRICS) {
				searchLyrics(true);
				return;
			}
		}

		getAlbumCover();
		setNoLyrics(response);
	}

	private void onGetLyrics(boolean isSuccess, String response) {
		mLoadLyrics.setVisibility(View.GONE);
		getAlbumCover();
		if(isSuccess && response.length() > 0) {
			try {
				String lyrics = response.substring(response.indexOf("<div class='lyricbox'>"));
				lyrics = lyrics.substring(lyrics.indexOf("</script>") + 9, lyrics.indexOf("<!--"));
				mLyrics = Html.fromHtml(lyrics).toString();
				mLyricsTxt.setVisibility(View.VISIBLE);
				mLyricsTxt.setText(mLyrics);
				return;
			} catch(StringIndexOutOfBoundsException e) {
				DebugLog.w(this, e.getMessage());
				e.printStackTrace();
			}
		}
		setNoLyrics(response);
	}

	private void setNoLyrics(String errorResponse) {
		mLoadLyrics.setVisibility(View.GONE);
		mNoLyricsTxt.setVisibility(View.VISIBLE);
		mNoLyricsTxt.setText(getResponseErrorMsg(errorResponse, "No lyrics found"));
	}

	private boolean isResponseFail(String errorResponse) {
		return errorResponse.toLowerCase().contains("timeoutexception");
	}

	private String getResponseErrorMsg(String errorResponse, String defaultErrorMsg) {
		if(errorResponse.toLowerCase().contains("exception")) return "Connection Error";
		return defaultErrorMsg;
	}

	private void getAlbumCover() {
		if(mTrack.albumImgUri != null && mTrack.albumImgUri.length() > 0) {
			new Thread(new Runnable() {
				public void run() {
					try {
						final Bitmap albumCover = BitmapFactory.decodeStream((InputStream) new URL(mTrack.albumImgUri).getContent());
						mMainView.post(new Runnable() {
							public void run() {
								searchInstrumentalTrack();
								((ImageView) mMainView.findViewById(R.id.img_album)).setImageBitmap(albumCover);
							}
						});
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	private void togglePlay() {
		DebugLog.d(this, isInitialized + " " + isActive + " " + isPlaying);
		if(isActive) {
			if(isPlaying) mPlayer.pause();
			else mPlayer.resume();
		} else playTrack();
	}

	private void startTrackPositionThread() {
		if(mTrackPositionThread == null) {
			mTrackPositionThread = new Thread() {
				public void run() {
					if(mPlayer != null) {
						mPlayer.getPlayerState(new PlayerStateCallback() {
							@Override
							public void onPlayerState(PlayerState playerState) {
								updateTrackView(playerState);
							}
						});
					}
					mTrackPositionThreadHandler.postDelayed(this, 1000);
				}
			};
		} else resumeTrackPositionThread();
	}

	private void stopTrackPositionThread() {
		if(mTrackPositionThread != null) {
			pauseTrackPositionThread();
			mTrackPositionThread.interrupt();
			mTrackPositionThread = null;
		}
	}

	private void resumeTrackPositionThread() {
		if(mTrackPositionThread != null) {
			mTrackPositionThreadHandler.removeCallbacks(mTrackPositionThread);
			mTrackPositionThreadHandler.postDelayed(mTrackPositionThread, 0);
		}
	}

	private void pauseTrackPositionThread() {
		if(mTrackPositionThread != null)
			mTrackPositionThreadHandler.removeCallbacks(mTrackPositionThread);
	}

	private void updateTimeTxt(long remainingTime) {
		int minutes = (int) Math.floor(remainingTime / 60000);
		int seconds = (int) Math.floor((remainingTime % 60000) / 1000);
		mTimeTxt.setText(FormatUtils.padZeros(minutes, 2) + ":" + FormatUtils.padZeros(seconds, 2));
	}

	@Override
	public void onLoggedIn() {
		DebugLog.w(this, "logged in");
	}

	@Override
	public void onLoggedOut() {
		DebugLog.w(this, "logged out");
	}

	@Override
	public void onLoginFailed(Throwable throwable) {
		DebugLog.w(this, "login failed");
		throwable.printStackTrace();
	}

	@Override
	public void onTemporaryError() {
		DebugLog.w(this, "temp error");
	}

	@Override
	public void onConnectionMessage(String text) {
		DebugLog.w(this, "connection: " + text);
	}

	@Override
	public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
		DebugLog.d(this, eventType + " " + playerState.playing + " " + playerState.positionInMs);
		isPlaying = playerState.playing;
		switch(eventType) {
			case TRACK_CHANGED:
				isActive = playerState.playing;
				break;
			case BECAME_ACTIVE:
				isInitialized = true;
				break;
			case BECAME_INACTIVE:
				isInitialized = false;
				break;
			case END_OF_CONTEXT:
				stopTrackPositionThread();
				mTrackSeek.setEnabled(false);
				break;
		}

		updateTrackView(playerState);
	}

	private void updateTrackView(PlayerState playerState) {
		try {
			if(playerState.playing) {
				if(mPrevPlayerState == null || !mPrevPlayerState.playing) {
					mPlayToggleBtn.setImageResource(R.drawable.ic_pause);
					mTimeTxt.setVisibility(View.VISIBLE);
					mLoadTrack.setVisibility(View.GONE);
					mTrackSeek.setEnabled(true);
					mTrackSeek.setMax(playerState.durationInMs);
					isSeeking = false;
				}

				startTrackPositionThread();
				updateTimeTxt(playerState.durationInMs - playerState.positionInMs);
				if(!isSeeking) mTrackSeek.setProgress(playerState.positionInMs);
			} else {
				if(mPrevPlayerState == null || mPrevPlayerState.playing) {
					mPlayToggleBtn.setImageResource(R.drawable.ic_play);
					pauseTrackPositionThread();
				}
			}
		} catch(Exception e) {
			DebugLog.eStackTrace(e);
		}
		mPrevPlayerState = playerState;
	}

	@Override
	public void onPlaybackError(ErrorType errorType, String text) {
		DebugLog.e(this, errorType + " " + text);
	}

	@Override
	public void onHttpResult(SingleHttpRequestTask request, int requestCode, boolean isSuccess, String response) {
		if(!isSuccess) DebugLog.e(this, response + "\n" + request.getRequestUri());
		switch(requestCode) {
			case REQUEST_SEARCH_INSTRUMENTAL:
				onSearchInstrumentalTrack(isSuccess, response);
				break;
			case REQUEST_SEARCH_LYRICS:
			case REQUEST_SEARCH_LYRICS_2:
				onSearchLyrics(requestCode, isSuccess, response);
				break;
			case REQUEST_GET_LYRICS:
				onGetLyrics(isSuccess, response);
				break;
		}
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_play_toggle) togglePlay();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		mCurrInstrumentalTrack = position;
		mTracksAdapter.setSelection(position);
		mTracksAdapter.notifyDataSetChanged();
		playTrack(mInstrumentalTracks.get(position));
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		if(view.getId() == R.id.lyrics && !mSlider.isOpen()) {
			mSlider.openPane();
			return true;
		}
		return false;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if(fromUser) try {
			mPlayer.seekToPosition(progress);
		} catch(Exception e) {
			DebugLog.eStackTrace(e);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		isSeeking = true;
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		isSeeking = false;
	}

	private Handler mPlayerHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			DebugLog.w(this, "handle: " + msg.toString());
		}
	};

	private BaseFragmentActivity.LifecycleObserver mActivityLifecycleObserver = new BaseFragmentActivity.LifecycleObserver() {
		@Override
		public void onResume() {}

		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {}

		@Override
		public void onNewIntent(Intent intent) {}

		@Override
		public void onSaveInstanceState(Bundle outState) {}

		@Override
		public void onStop() {}

		@Override
		public void onDestroy() {}

		@Override
		public boolean onBackPressed() {
			if(!mSlider.isOpen()) {
				mSlider.openPane();
				return false;
			}
			return true;
		}
	};
}
