package com.yondu.spotifykaraoke.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SlidingPaneLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.BaseFragmentActivity;
import com.yondu.base.utils.FormatUtils;
import com.yondu.base.utils.SingleHttpRequestTask;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.model.App;
import com.yondu.spotifykaraoke.model.Track;
import com.yondu.spotifykaraoke.model.User;
import com.yondu.spotifykaraoke.model.VoiceRecord;
import com.yondu.spotifykaraoke.utils.DebugLog;
import com.yondu.spotifykaraoke.utils.SearchHelper;
import com.yondu.spotifykaraoke.widgets.TrackListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 5/15/2015.
 */
@SuppressLint("ValidFragment")
public class RecordFragment extends BaseActionBarFragment implements View.OnClickListener {


	private View mMainView;
	private VoiceRecord mVoiceRecord;

	public RecordFragment() {}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.fragment_record, container, false);
		initView();
		return mMainView;
	}

	@Override
	public void onPause() {
		mVoiceRecord.release();
		super.onPause();
	}

	private void initView() {
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mVoiceRecord = new VoiceRecord();
		mMainView.findViewById(R.id.btn_record).setOnClickListener(this);
		mMainView.findViewById(R.id.btn_play).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_record) mVoiceRecord.toggleRecord();
		else if(view.getId() == R.id.btn_play) mVoiceRecord.togglePlay();
	}
}
