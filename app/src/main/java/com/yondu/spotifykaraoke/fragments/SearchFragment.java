package com.yondu.spotifykaraoke.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.utils.SingleHttpRequestTask;
import com.yondu.spotifykaraoke.R;
import com.yondu.spotifykaraoke.model.Track;
import com.yondu.spotifykaraoke.utils.DebugLog;
import com.yondu.spotifykaraoke.utils.SearchHelper;
import com.yondu.spotifykaraoke.widgets.TrackListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 5/4/2015.
 */
public class SearchFragment extends BaseActionBarFragment implements TextWatcher, AdapterView.OnItemClickListener, View.OnClickListener, SingleHttpRequestTask.OnHttpRequestListener {

	private static final long SEARCH_INTERVAL_MILLIS = 1000;

	private String mPrevSearchString = "";
	private View mMainView, mNoResutsView, mSearchLoader;
	private TextView mTrackInput, mSearchStatusTxt;
	private WeakReference<Activity> mWeakActivity;
	private ListView mSearchList;
	private TrackListAdapter mTracksAdapter;
	private List<Track> mTrackList = new ArrayList<Track>();
	private List<SingleHttpRequestTask> mSearchRequests = new ArrayList<SingleHttpRequestTask>();
	private SearchRequestCountdown mSearchRequestCountdown;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.fragment_search, container, false);
		mWeakActivity = new WeakReference<Activity>(getActivity());
		initView();
		return mMainView;
	}

	@Override
	public void onPause() {
		super.onPause();
		clearSearchTasks(-1);
		stopSearchRequestCountdown();
	}

	private void initView() {
		getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getBaseActionBar().setTitleText("Search");

		mSearchLoader = mMainView.findViewById(R.id.load_search);
		mSearchStatusTxt = (TextView) mMainView.findViewById(R.id.txt_search_status);
		mNoResutsView = mMainView.findViewById(R.id.txt_no_results);

		mTrackInput = (TextView) mMainView.findViewById(R.id.input_track);
		mTrackInput.addTextChangedListener(this);
		mMainView.findViewById(R.id.btn_clear).setOnClickListener(this);

		mSearchList = (ListView) mMainView.findViewById(R.id.list_search);
		mTracksAdapter = new TrackListAdapter(getContext(), R.layout.view_search_item, mTrackList);
		mSearchList.setAdapter(mTracksAdapter);
		mSearchList.setOnItemClickListener(this);
		if(mTrackList.size() == 0) showSampleList();
	}

	private void updateView() {
//		mNoResutsView.setVisibility(mTrackList.size() > 0 ? View.GONE : View.VISIBLE);
	}

	private void showSampleList() {
		mTrackList.clear();
		List<Track> sampleList = Track.getSampleTracks();
		for(int i = 0; i < sampleList.size(); i++) mTrackList.add(sampleList.get(i));
		mTracksAdapter.notifyDataSetChanged();
		mSearchList.smoothScrollToPosition(0);
		mSearchLoader.setVisibility(View.GONE);
		mSearchStatusTxt.setText("Instrumental Tracks");
		updateView();
	}

	private void startSearch() {
		if(!mPrevSearchString.contentEquals(mTrackInput.getText())) {
			mSearchLoader.setVisibility(View.VISIBLE);
			mSearchStatusTxt.setText("Searching...");
			String searchString = mTrackInput.getText().toString();
			if(searchString.length() > 0) {
//				startSearchRequestCountdown(SEARCH_INTERVAL_MILLIS);
				search();
			} else {
				showSampleList();
				clearSearchTasks(-1);
			}
		}
		mPrevSearchString = mTrackInput.getText().toString();
	}

	private SingleHttpRequestTask searchRequest;
	private void search() {
		String searchString = SearchHelper.generateSearchUri(mTrackInput.getText().toString());
		if(searchRequest != null && searchRequest.isActive()) searchRequest.cancel(true);
		searchRequest = new SingleHttpRequestTask(0, mWeakActivity, this);
		searchRequest.execute(new SingleHttpRequestTask.RequestParams(searchString));
//		searchRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(searchString));
		mSearchRequests.add(searchRequest);
		DebugLog.i(this, searchString);
	}

	private void resetSearch() {
		mTrackInput.setText("");
	}

	private boolean isAllSearchDone() {
		return mSearchRequests.size() == 0;
	}

	private void onSearchResults(boolean isSuccess, String response) {
		if(response.length() > 0 && mTrackInput.getText().length() > 0) {
			try {
//				JSONObject tracksJson = new JSONObject(response).getJSONObject("tracks");
//				if(tracksJson.getInt("total") > 0) updateList(tracksJson.getJSONArray("items"));
				JSONObject resultJson = new JSONObject(response);
				if(resultJson.getInt("resultCount") > 0) updateList(resultJson.getJSONArray("results"));
				else {
					mTrackList.clear();
					mTracksAdapter.notifyDataSetChanged();
					if(isAllSearchDone()) {
						mSearchLoader.setVisibility(View.GONE);
						mSearchStatusTxt.setText("No Results Found");
					}
					updateView();
				}
				return;
			} catch(JSONException e) {
				e.printStackTrace();
			}
		}
		if(isAllSearchDone()) {
			mSearchLoader.setVisibility(View.GONE);
			mSearchStatusTxt.setText("Connection Error");
		}
	}

	private void clearSearchTasks(int lastIndex) {
		if(lastIndex == -1) lastIndex = mSearchRequests.size() - 1;
		for(int i = lastIndex; i >= 0; i--) {
			if(i < lastIndex) mSearchRequests.get(i).cancel(true);
			mSearchRequests.remove(i);
		}
	}

	private void updateList(JSONArray results) throws JSONException {
		mTrackList.clear();
//		for(int i = 0; i < results.length(); i++) {
//			JSONObject result = results.getJSONObject(i);
//			String title = result.getString("name");
//			String artists = "";
//
//			JSONArray artistsJson = result.getJSONArray("artists");
//			for(int j = 0; j < artistsJson.length(); j++)
//				artists += (artists.length() > 0 ? ", " : "") + artistsJson.getJSONObject(j).getString("name");
//
//			JSONArray images = result.getJSONObject("album").getJSONArray("images");
//			String albumUri = images.length() > 1 ? result.getJSONObject("album").getJSONArray("images").getJSONObject(1).getString("url") : "";
//			mTrackList.add(new Track(title, artists, null, albumUri));
//		}
		for(int i = 0; i < results.length(); i++) {
			JSONObject result = results.getJSONObject(i);
			String title = result.getString("trackName");
			String artists = result.getString("artistName");
			String albumArt = result.getString("artworkUrl100").replace("100x100", "300x300");
			mTrackList.add(new Track(title, artists, null, albumArt));
		}
		mTracksAdapter.notifyDataSetChanged();
		mSearchList.smoothScrollToPosition(0);
		if(isAllSearchDone()) {
			mSearchLoader.setVisibility(View.GONE);
			mSearchStatusTxt.setText("Search Results");
		}
		updateView();
	}

	private void startSearchRequestCountdown(long remainingTime) {
		stopSearchRequestCountdown();
		mSearchRequestCountdown = new SearchRequestCountdown(remainingTime, 500);
		mSearchRequestCountdown.start();
	}

	private void stopSearchRequestCountdown() {
		if(mSearchRequestCountdown != null) mSearchRequestCountdown.cancel();
	}

	@Override
	public void onHttpResult(SingleHttpRequestTask request, int requestCode, boolean isSuccess, String response) {
		if(!isSuccess) DebugLog.e(this, response + "\n" + request.getRequestUri());
		clearSearchTasks(mSearchRequests.indexOf(request));
		onSearchResults(isSuccess, response);
	}

	@Override
	public void afterTextChanged(Editable editable) {
		startSearch();
	}

	@Override
	public void beforeTextChanged(CharSequence text, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence text, int start, int before, int count) {
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mTrackInput.getWindowToken(), 0);
		pushFragment(new TrackFragment(mTrackList.get(position)));
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_clear) resetSearch();
	}

	private class SearchRequestCountdown extends CountDownTimer {

		public SearchRequestCountdown(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished) {}

		@Override
		public void onFinish() {
			search();
		}

	}
}
