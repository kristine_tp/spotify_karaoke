package com.yondu.base.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yondu.applicationtemplate.R;

public class SimpleAlertFragmentDialog extends BaseFragmentDialog implements OnClickListener {

	public enum DialogStyle {
		PANEL, FULLSCREEN
	}

	private DialogStyle mStyle;
	private Resources mResources;
	private View mContentView;
	private OnDialogResultListener mResultListener;

	private boolean mIsCancelable = true;
	private String mTitle, mMessage, mPositiveText, mNegativeText;
	private View mCustomView;

	public SimpleAlertFragmentDialog(Context context) {
		this(context, DialogStyle.PANEL);
	}

	public SimpleAlertFragmentDialog(Context context, DialogStyle style) {
		super(null);
		mResources = context.getResources();
		mStyle = style;
	}

	public void setTitle(int stringResId) {
		mTitle = mResources.getString(stringResId);
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public void setMessage(int stringResId) {
		mMessage = mResources.getString(stringResId);
	}

	public void setMessage(String message) {
		mMessage = message;
	}

	public void setCustomView(View customView) {
		mCustomView = customView;
	}

	public void setPositiveButton(int textResId) {
		mPositiveText = mResources.getString(textResId);
	}

	public void setPositiveButton(String text) {
		mPositiveText = text;
	}

	public void setNegativeButton(int textResId) {
		mNegativeText = mResources.getString(textResId);
	}

	public void setNegativeButton(String text) {
		mNegativeText = text;
	}

	public void setResultListener(OnDialogResultListener resultListener) {
		mResultListener = resultListener;
	}

	@Override
	public void setCancelable(boolean isCancelable) {
		super.setCancelable(isCancelable);
		mIsCancelable = isCancelable;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initViews();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setDialogAttrs();
	}

	private void setDialogAttrs() {
		switch (mStyle) {
		case FULLSCREEN:
			getView().setBackgroundResource(R.color.dialog_fullscreen_bg);
			break;
		case PANEL:
			if (mIsCancelable) getView().findViewById(R.id.dialog_outside).setOnClickListener(this);
			break;
		}
	}

	@SuppressLint("InflateParams")
	private void initViews() {
		mContentView = LayoutInflater.from(getActivity()).inflate(mStyle == DialogStyle.PANEL ? R.layout.view_alert_dialog_panel : R.layout.view_alert_dialog_fullscreen, null);
		setValues();
		setContentView(mContentView);
	}

	private void setValues() {
		setViewValue(mTitle, R.id.txt_title);
		if (mCustomView != null) {
			((ViewGroup) mContentView.findViewById(R.id.grp_custom)).addView(mCustomView);
			mContentView.findViewById(R.id.grp_standard).setVisibility(View.GONE);
		}
		else setViewValue(mMessage, R.id.text_message);

		if (setViewValue(mPositiveText, R.id.btn_dialog_positive)) mContentView.findViewById(R.id.btn_dialog_positive).setOnClickListener(this);
		if (setViewValue(mNegativeText, R.id.btn_dialog_negative)) mContentView.findViewById(R.id.btn_dialog_negative).setOnClickListener(this);
	}

	private boolean setViewValue(String value, int viewResId) {
		if (value != null) {
			((TextView) mContentView.findViewById(viewResId)).setText(value);
			mContentView.findViewById(viewResId).setVisibility(View.VISIBLE);
			return true;
		}
		return false;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		if (mResultListener != null) mResultListener.onDialogResult(this, false);
	}

	@Override
	public void onClick(View view) {
		if (mResultListener != null) {
			if (view.getId() == R.id.btn_dialog_positive) mResultListener.onDialogResult(this, true);
			if (view.getId() == R.id.btn_dialog_negative || view.getId() == R.id.dialog_outside) mResultListener.onDialogResult(this, false);
		}
		dismiss();
	}

	public interface OnDialogResultListener {
		void onDialogResult(SimpleAlertFragmentDialog dialog, boolean isSuccess);
	}

}
