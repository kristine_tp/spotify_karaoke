package com.yondu.base.views;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.yondu.base.app.BaseFragmentActivity;
import com.yondu.base.utils.DebugLog;

public class TabActivities extends LinearLayout implements OnClickListener {

	private static int mCurrTab;
	private static Class<?>[] mTabActivities;
	private WeakReference<Activity> mWeakActivity;

	public void initTabActivities(Class<?>... tabActivityClasses) {
		mCurrTab = 0;
		mTabActivities = tabActivityClasses;
	}

	public void init(WeakReference<Activity> weakActivity) {
		mWeakActivity = weakActivity;
		for (int i = 0; i < getChildCount(); i++)
			getChildAt(i).setOnClickListener(this);
		updateTabs();
	}

	public void switchHomeTab() {
		switchTab(0);
	}

	public void switchTab(int tabIndex) {
		try {
			Class<?> activityClass = mTabActivities[tabIndex];
			Activity activity = mWeakActivity.get();
			if (activityClass != null && activity != null) {
				activity.startActivity(new Intent(activity.getApplicationContext(), activityClass));
				if (mCurrTab == tabIndex) ((BaseFragmentActivity) activity).backToFirstFragment();
			}

			mCurrTab = tabIndex;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}

		updateTabs();
	}

	public void updateTabs() {
		for (int i = 0; i < getChildCount(); i++)
			getChildAt(i).setSelected(false);
		getChildAt(mCurrTab).setSelected(true);
	}

	@Override
	public void onClick(View view) {
		switchTab(indexOfChild(view));
	}

	public TabActivities(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public TabActivities(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public TabActivities(Context context) {
		super(context);
	}

}