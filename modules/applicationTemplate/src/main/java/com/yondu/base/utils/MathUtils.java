package com.yondu.base.utils;

import java.util.Random;

public class MathUtils {

	public static int getRandom(int max) {
		return new Random().nextInt(max);
	}

	public static int getRandom(int min, int max) {
		return new Random().nextInt(max - min) + min;
	}

	public static String getRandomDigits(int digitCount) {
		String digits = "";
		for (int i = 0; i < digitCount; i++) {
			digits += String.valueOf(new Random().nextInt(10));
		}
		return digits;
	}

}
