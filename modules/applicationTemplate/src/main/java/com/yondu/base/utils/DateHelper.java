package com.yondu.base.utils;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	private static final String BASIC_DATE_FORMAT = "MM/dd/yyyy";

	public static String getBasicFormattedDate(Calendar date) {
		return getFormattedDate(date, BASIC_DATE_FORMAT);
	}

	@SuppressLint("SimpleDateFormat")
	public static String getFormattedDate(String dateFormat) {
		return getFormattedDate(Calendar.getInstance(), dateFormat);
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String getFormattedDate(Calendar date, String dateFormat) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(dateFormat);
		return simpleFormat.format(date.getTime());
	}
}
