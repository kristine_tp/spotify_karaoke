package com.yondu.base.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;

import com.yondu.base.utils.DebugLog;

public class BaseFragment extends Fragment {

	private static final boolean IS_LIFECYCLE_DEBUG = false;
	private boolean mIsAttached;

	protected Context getContext() {
		return getActivity().getApplicationContext();
	}

	public boolean isAttached() {
		return mIsAttached;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "attach");
		mIsAttached = true;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "create");
	}

	@Override
	public void onStart() {
		super.onStart();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "start");
	}

	@Override
	public void onResume() {
		super.onResume();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "resume");
	}

	@Override
	public void onPause() {
		super.onPause();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "pause");
	}

	@Override
	public void onStop() {
		super.onStop();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "stop");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "destroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if(IS_LIFECYCLE_DEBUG) DebugLog.w(this, "detach");
	}

}
