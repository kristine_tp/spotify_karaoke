package com.yondu.base.app;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.yondu.applicationtemplate.R;
import com.yondu.base.app.BaseActionBar.ActionButton;
import com.yondu.base.app.BaseActionBar.ActionHome;

public class SimpleMenuDrawerActivity extends SimpleActionBarActivity {

	private BaseMenuDrawer mMenuDrawer;
	private View mMenuDrawerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initMenuDrawer();
	}

	@Override
	protected void onBaseSetContentView() {
		setContentView(R.layout.activity_menu_drawer);
		setLoadingView(R.id.main_loading);
	}

	@Override
	public void setContentView(View view, LayoutParams params) throws NullPointerException {
		super.setContentView(view, params);
		onSetContentView();
	}

	public void setMenuDrawerView(View view) {
		mMenuDrawerView = view;
	}

	private void initMenuDrawer() {
		mMenuDrawer = new BaseMenuDrawer(this, R.id.drawer_container, R.id.menu_drawer, mMenuDrawerView);
	}

	public BaseMenuDrawer getMenuDrawer() {
		return mMenuDrawer;
	}

	@Override
	public void onRightActionButtonClicked(ActionButton which) {
		mMenuDrawer.closeDrawer();
		super.onRightActionButtonClicked(which);
	}

	@Override
	public void onHomeButtonClicked(ActionHome homeMode) {
		if (homeMode == ActionHome.HOME) mMenuDrawer.toggleDrawer();
		else super.onHomeButtonClicked(homeMode);
	}

}
