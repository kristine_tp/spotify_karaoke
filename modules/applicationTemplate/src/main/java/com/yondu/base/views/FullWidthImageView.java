package com.yondu.base.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.yondu.base.utils.AppUtils;
import com.yondu.base.utils.DebugLog;

public class FullWidthImageView extends ImageView {

	public FullWidthImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		fitScreenWidth();
	}

	private void fitScreenWidth() {
		int width = AppUtils.getScreenSize(getContext()).x;
		float scaleRatio = (float) width / (float) getDrawable().getIntrinsicWidth();
		DebugLog.d(this, getDrawable().getIntrinsicWidth() + " " + scaleRatio);
		int height = (int) (getDrawable().getIntrinsicHeight() * scaleRatio);
		getLayoutParams().width = width;
		getLayoutParams().height = height;
	}

}
