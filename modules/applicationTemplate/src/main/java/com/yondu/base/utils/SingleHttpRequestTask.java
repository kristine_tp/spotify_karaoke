package com.yondu.base.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.os.AsyncTask;

import com.yondu.base.utils.SingleHttpRequestTask.RequestParams;
import com.yondu.base.utils.SingleHttpRequestTask.RequestResult;

public class SingleHttpRequestTask extends AsyncTask<RequestParams, Void, RequestResult> {

	private static final int CONNECTION_TIMEOUT = 5000;
	private static final int SOCKET_TIMEOUT = 5000;

	private int mRequestCode;
	private boolean mIsCompleted;
	private String mRequestUri;
	private WeakReference<Activity> mWeakActivity;
	private OnHttpRequestListener mRequestListener;

	public SingleHttpRequestTask(int requestCode, WeakReference<Activity> weakActivity, OnHttpRequestListener requestListener) {
		Connectivity.isConnected(weakActivity, true);
		mRequestCode = requestCode;
		mWeakActivity = weakActivity;
		mRequestListener = requestListener;
	}

	public boolean isCompleted() {
		return mIsCompleted;
	}

	public boolean isActive() {
		return !isCancelled() && !mIsCompleted;
	}

	@Override
	protected RequestResult doInBackground(RequestParams... params) {
		DebugLog.i(this, "Uri: " + params[0].uri);
		if(params[0].vars == null || params[0].vars.size() == 0) return getRequest(params[0].uri);
		else return postRequest(params[0]);
	}

	@Override
	protected void onPostExecute(RequestResult result) {
		DebugLog.v(this, "response: " + result.response);
		mIsCompleted = true;
		if(!result.isSuccess) Connectivity.isConnected(mWeakActivity);
		if(mRequestListener != null)
			mRequestListener.onHttpResult(this, mRequestCode, result.isSuccess, result.response);
	}

	private RequestResult getRequest(String uri) {
		DebugLog.i(this, "get: " + uri);

		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpParams httpParams = httpClient.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT);
			HttpConnectionParams.setLinger(httpParams, CONNECTION_TIMEOUT);

			HttpGet httpGet = new HttpGet(uri);
			HttpResponse response;

			response = httpClient.execute(httpGet);
			return new RequestResult(true, EntityUtils.toString(response.getEntity()));
		} catch(ClientProtocolException e) {
			DebugLog.eStackTrace(e);
			return new RequestResult(false, e.toString());
		} catch(IOException e) {
			DebugLog.eStackTrace(e);
			return new RequestResult(false, e.toString());
		} catch(Exception e) {
			DebugLog.eStackTrace(e);
			return new RequestResult(false, e.toString());
		}
	}

	private RequestResult postRequest(RequestParams params) {
		DebugLog.v(this, "post: " + params.uri);
		for(int i = 0; i < params.vars.size(); i++)
			DebugLog.d(this, params.vars.get(i).getName() + " = " + params.vars.get(i).getValue());

		HttpClient httpClient = new DefaultHttpClient();
		HttpParams httpParams = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParams, SOCKET_TIMEOUT);
		HttpConnectionParams.setLinger(httpParams, CONNECTION_TIMEOUT);
		HttpPost httpPost = new HttpPost(params.uri);

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params.vars));
			try {
				HttpResponse response = httpClient.execute(httpPost);
				return new RequestResult(true, EntityUtils.toString(response.getEntity()));
			} catch(ClientProtocolException e) {
				DebugLog.eStackTrace(e);
				return new RequestResult(false, e.toString());
			} catch(IOException e) {
				DebugLog.eStackTrace(e);
				return new RequestResult(false, e.toString());
			} catch(Exception e) {
				DebugLog.eStackTrace(e);
				return new RequestResult(false, e.toString());
			}
		} catch(UnsupportedEncodingException e) {
			DebugLog.eStackTrace(e);
			return new RequestResult(false, e.toString());
		}
	}

	public String getRequestUri() {
		return mRequestUri;
	}

	public static class RequestParams {
		private String uri;
		private List<NameValuePair> vars;

		public RequestParams(String uri, NameValuePair... vars) {
			this(uri, new ArrayList<NameValuePair>(Arrays.asList(vars)));
		}

		public RequestParams(String uri, List<NameValuePair> vars) {
			this.uri = uri;
			this.vars = vars;
		}
	}

	public class RequestResult {
		public boolean isSuccess;
		public String response;

		public RequestResult(boolean isSuccess, String response) {
			this.isSuccess = isSuccess;
			this.response = response;
		}
	}

	public interface OnHttpRequestListener {
		void onHttpResult(SingleHttpRequestTask request, int requestCode, boolean isSuccess, String response);
	}
}
