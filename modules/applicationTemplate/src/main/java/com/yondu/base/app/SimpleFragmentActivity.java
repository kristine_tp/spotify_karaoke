package com.yondu.base.app;

import android.os.Bundle;

import com.yondu.applicationtemplate.R;

public class SimpleFragmentActivity extends BaseActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onBaseSetContentView() {
		setContentView(R.layout.activity_base);
		setLoadingView(R.id.main_loading);
	}

}
