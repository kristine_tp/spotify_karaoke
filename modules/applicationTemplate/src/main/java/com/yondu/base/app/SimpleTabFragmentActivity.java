package com.yondu.base.app;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.os.Bundle;

import com.yondu.applicationtemplate.R;
import com.yondu.base.views.TabActivities;

public class SimpleTabFragmentActivity extends SimpleActionBarActivity {

	private static boolean mIsExit;
	private boolean mIsHomeTab;
	private TabActivities mTabs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mIsExit = false;
	}

	@Override
	public void onResume() {
		super.onResume();
		mTabs.updateTabs();
		if (mIsExit) finish();
	}

	@Override
	protected void onBaseSetContentView() {
		setContentView(R.layout.activity_tabs);
	}

	@Override
	protected void onSetContentView() {
		super.onSetContentView();
		mTabs = (TabActivities) findViewById(R.id.activity_tabs);
		if (mTabs == null) {
			finish();
			throw new NullPointerException("ContentView must contain a BaseFragmentTabs with id R.id.activity_tabs");
		}
		mTabs.init(new WeakReference<Activity>(this));
	}

	protected void initTabActivities(Class<?>... tabActiviyClasses) {
		try {
			if (getClass().equals(tabActiviyClasses[0])) mIsHomeTab = true;
			mTabs.initTabActivities(tabActiviyClasses);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		if (getBaseFragmentManager().getBackStackEntryCount() == 0) {
			if (mIsHomeTab) mIsExit = true;
			else mTabs.switchHomeTab();
		}
		super.onBackPressed();
	}
}
