package com.yondu.base.utils;

import android.util.Log;

public class DebugLog {

	private static final String TAG = "yondu-base";
	private static final boolean IS_DEBUG = true;

	public static void v(Object obj, String msg) {
		if (IS_DEBUG) Log.v(TAG, msg + " (" + obj.getClass().getSimpleName() + "." + obj.hashCode() + ")");
	}

	public static void d(Object obj, String msg) {
		if (IS_DEBUG) Log.d(TAG, msg + " (" + obj.getClass().getSimpleName() + "." + obj.hashCode() + ")");
	}

	public static void i(Object obj, String msg) {
		if (IS_DEBUG) Log.i(TAG, msg + " (" + obj.getClass().getSimpleName() + "." + obj.hashCode() + ")");
	}

	public static void w(Object obj, String msg) {
		if (IS_DEBUG) Log.w(TAG, msg + " (" + obj.getClass().getSimpleName() + "." + obj.hashCode() + ")");
	}

	public static void e(Object obj, String msg) {
		if (IS_DEBUG) Log.e(TAG, msg + " (" + obj.getClass().getSimpleName() + "." + obj.hashCode() + ")");
	}

	public static void eStackTrace(Exception e) {
		Log.e(TAG, e.toString());
		e.printStackTrace();
	}

}
