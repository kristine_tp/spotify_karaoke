package com.yondu.base.app;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup.LayoutParams;

import com.yondu.base.app.BaseActionBar.ActionButton;
import com.yondu.base.app.BaseActionBar.ActionHome;
import com.yondu.base.app.BaseActionBar.BaseActionBarListener;

public class BaseActionBarActivity extends BaseFragmentActivity implements BaseActionBarListener {

	private BaseActionBar mBaseActionBar;

	@Override
	protected void onBaseCreate() {
		initActionBar();
		super.onBaseCreate();
	}

	protected void onInitActionBar() {
	}

	private void initActionBar() {
		mBaseActionBar = new BaseActionBar(this, this);
		ActionBar actionBar = getSupportActionBar();
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(mBaseActionBar.getView(), new ActionBar.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		Toolbar parent = (Toolbar) mBaseActionBar.getView().getParent();
		parent.setContentInsetsAbsolute(0,0);
		onInitActionBar();
	}

	public BaseActionBar getBaseActionBar() {
		return mBaseActionBar;
	}

	@Override
	public void onRightActionButtonClicked(ActionButton which) {
	}

	@Override
	public void onHomeButtonClicked(ActionHome homeMode) {
		switch (homeMode) {
		case HOME:
			backToFirstFragment();
			break;
		case BACK:
			getBaseFragmentManager().popBackStack();
			break;
		default:
			break;
		}
	}

}
