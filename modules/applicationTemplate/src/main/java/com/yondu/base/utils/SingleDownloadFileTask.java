package com.yondu.base.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;

public class SingleDownloadFileTask extends AsyncTask<String, Void, Boolean> {

	private String mOutputPath;
	private OnDownloadFileListener mDownloadListener;

	public SingleDownloadFileTask(String outputPath, OnDownloadFileListener downloadListener) {
		mOutputPath = outputPath;
		mDownloadListener = downloadListener;
	}

	@Override
	protected Boolean doInBackground(String... uris) {
		DebugLog.i(this, "url: " + uris[0]);
		return getFile(uris[0]);
	}

	@Override
	protected void onPostExecute(Boolean isSuccess) {
		if (mDownloadListener != null) mDownloadListener.onDownloadFile(isSuccess, mOutputPath);
	}

	private boolean getFile(String uri) {
		new File(mOutputPath).mkdirs();
		File output = new File(mOutputPath);
		if (output.exists()) output.delete();

		try {
			URL url = new URL(uri);
			URLConnection conn = url.openConnection();
			int contentLength = conn.getContentLength();
			DataInputStream stream = new DataInputStream(url.openStream());

			byte[] buffer = new byte[contentLength];
			stream.readFully(buffer);
			stream.close();

			DataOutputStream fos = new DataOutputStream(new FileOutputStream(output.getPath()));
			fos.write(buffer);
			fos.flush();
			fos.close();
			return true;
		} catch (Exception e) {
			DebugLog.eStackTrace(e);
			return false;
		}

	}

	public interface OnDownloadFileListener {
		void onDownloadFile(boolean isSuccess, String outputPath);
	}
}
