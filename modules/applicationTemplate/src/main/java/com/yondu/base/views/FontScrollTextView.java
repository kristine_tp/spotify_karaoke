package com.yondu.base.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.yondu.applicationtemplate.R;

public class FontScrollTextView extends TextView {

	public FontScrollTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public FontScrollTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public FontScrollTextView(Context context) {
		super(context);
	}

	private void init(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);
			String font = array.getString(R.styleable.FontTextView_font);
			if (font != null && !isInEditMode()) {
				Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font);
				setTypeface(type);
			}
			array.recycle();
		}
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
		if (focused) {
			super.onFocusChanged(focused, direction, previouslyFocusedRect);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean focused) {
		if (focused) {
			super.onWindowFocusChanged(focused);
		}
	}

	@Override
	public boolean isFocused() {
		return true;
	}
}
