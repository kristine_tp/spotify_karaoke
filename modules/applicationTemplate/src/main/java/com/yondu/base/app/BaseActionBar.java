package com.yondu.base.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.applicationtemplate.R;

public class BaseActionBar implements OnClickListener {

	public enum ActionButton {
		BUTTON_LEFT, BUTTON_RIGHT, BUTTON_RIGHT_2
	};

	public enum ActionHome {
		HOME, BACK
	}

	private ActionHome mActionHome = ActionHome.HOME;
	private Context mContext;
	private View mActionBarView;
	private BaseActionBarListener mListener;

	@SuppressLint("InflateParams")
	public BaseActionBar(Activity activity, BaseActionBarListener listener) {
		mContext = activity.getApplicationContext();
		mListener = listener;
		mActionBarView = LayoutInflater.from(mContext).inflate(R.layout.view_actionbar, null);
	}

	public View getView() {
		return mActionBarView;
	}

	public void showButton(ActionButton button, int imgResId) {
		ImageView buttonView = getButtonView(button);
		buttonView.setVisibility(View.VISIBLE);
		buttonView.setImageResource(imgResId);
		buttonView.setOnClickListener(this);
	}

	private ImageView getButtonView(ActionButton button) {
		switch (button) {
		case BUTTON_LEFT:
			return (ImageView) mActionBarView.findViewById(R.id.btn_actionbar_left);
		case BUTTON_RIGHT:
			return (ImageView) mActionBarView.findViewById(R.id.btn_actionbar_right);
		case BUTTON_RIGHT_2:
			return (ImageView) mActionBarView.findViewById(R.id.btn_actionbar_right_2);
		}
		return null;
	}

	private ActionButton getActionButton(int resId) {
		if (resId == R.id.btn_actionbar_left) return ActionButton.BUTTON_LEFT;
		else if (resId == R.id.btn_actionbar_right) return ActionButton.BUTTON_RIGHT;
		else if (resId == R.id.btn_actionbar_right_2) return ActionButton.BUTTON_RIGHT_2;
		return null;
	}

	public void setHomeAction(ActionHome homeMode, int imgResId) {
		mActionHome = homeMode;
		showButton(ActionButton.BUTTON_LEFT, imgResId);
	}

	public void setTitleText(int textResId) {
		setTitleText(mContext.getResources().getString(textResId));
	}

	public void setTitleText(String text) {
		clearTitleContent();
		((TextView) mActionBarView.findViewById(R.id.txt_title)).setText(text);
	}

	public void setTitleImg(int imgResId) {
		clearTitleContent();
		((ImageView) mActionBarView.findViewById(R.id.img_title)).setImageResource(imgResId);
	}

	public void setBackgroundColor(int color) {
		mActionBarView.setBackgroundColor(color);
	}

	public void setBackgroundResource(int drawableResId) {
		mActionBarView.setBackgroundResource(drawableResId);
	}

	public void setTitleView(View view) {
		clearTitleContent();
		((ViewGroup) mActionBarView.findViewById(R.id.title_container)).addView(view);
	}

	private void clearTitleContent() {
		((TextView) mActionBarView.findViewById(R.id.txt_title)).setText("");
		((ImageView) mActionBarView.findViewById(R.id.img_title)).setImageResource(0);
		((ViewGroup) mActionBarView.findViewById(R.id.title_container)).removeAllViews();
	}

	@Override
	public void onClick(View view) {
		ActionButton button = getActionButton(view.getId());
		if (button == ActionButton.BUTTON_LEFT) mListener.onHomeButtonClicked(mActionHome);
		else mListener.onRightActionButtonClicked(getActionButton(view.getId()));
	}

	public interface BaseActionBarListener {
		void onRightActionButtonClicked(ActionButton which);

		void onHomeButtonClicked(ActionHome homeMode);
	}

}
