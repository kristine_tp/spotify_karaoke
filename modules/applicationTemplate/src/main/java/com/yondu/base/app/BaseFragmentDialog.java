package com.yondu.base.app;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yondu.applicationtemplate.R;

public class BaseFragmentDialog extends DialogFragment {

	private View mContentView;
	private ViewGroup mContainerView;

	public BaseFragmentDialog(View contentView) {
		setStyle(STYLE_NO_FRAME, 0);
		mContentView = contentView;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = (ViewGroup) inflater.inflate(R.layout.fragment_dialog, null);
		mContainerView = (ViewGroup) view.findViewById(R.id.dialog_container);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (mContentView != null) setContentView(mContentView);
	}

	public void setContentView(View customView) {
		mContentView = customView;
		if (mContainerView != null) mContainerView.addView(mContentView);
	}
}
