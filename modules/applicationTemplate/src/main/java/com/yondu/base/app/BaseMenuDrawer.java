package com.yondu.base.app;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

public class BaseMenuDrawer {

	private DrawerLayout mDrawerContainer;
	private View mMenuView;

	public BaseMenuDrawer(AppCompatActivity activity, int drawerContainerResId, int drawerViewResId, View drawerContent) {
		mDrawerContainer = (DrawerLayout) activity.findViewById(drawerContainerResId);
		mMenuView = activity.findViewById(drawerViewResId);
		if (drawerContent != null) ((ViewGroup) mMenuView).addView(drawerContent);
	}

	public void toggleDrawer() {
		if (isDrawerOpen()) closeDrawer();
		else openDrawer();
	}

	public void openDrawer() {
		mDrawerContainer.openDrawer(mMenuView);
	}

	public void closeDrawer() {
		mDrawerContainer.closeDrawer(mMenuView);
	}

	public boolean isDrawerOpen() {
		return mDrawerContainer.isDrawerOpen(mMenuView) || mDrawerContainer.isDrawerVisible(mMenuView);
	}

	public void enableSwipeDrawer(boolean isEnabled) {
		mDrawerContainer.setDrawerLockMode(isEnabled ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}
}
