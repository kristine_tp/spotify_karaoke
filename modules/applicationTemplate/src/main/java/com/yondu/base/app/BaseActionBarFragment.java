package com.yondu.base.app;

public class BaseActionBarFragment extends BaseActivityFragment {

	protected BaseActionBar getBaseActionBar() {
		try {
			return ((BaseActionBarActivity) getBaseFragmentActivity()).getBaseActionBar();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return null;
		}
	}

}
