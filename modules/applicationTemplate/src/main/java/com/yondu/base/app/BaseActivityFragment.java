package com.yondu.base.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class BaseActivityFragment extends BaseFragment {

	private BaseFragmentActivity mActivity;

	public void asTopFragment(int backStackCount) {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BaseFragmentActivity) getActivity(); 
	}

	protected BaseFragmentActivity getBaseFragmentActivity() {
		return mActivity;
	}
	
	protected void finish() {
		mActivity.backToPreviousFragment();
	}
	
	protected void pushFragment(Fragment fragment) {
		mActivity.pushContentFragment(fragment);
	}

}
