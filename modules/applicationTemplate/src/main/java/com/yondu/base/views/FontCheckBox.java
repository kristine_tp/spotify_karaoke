package com.yondu.base.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.yondu.applicationtemplate.R;

public class FontCheckBox extends CheckBox {

	public FontCheckBox(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public FontCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public FontCheckBox(Context context) {
		super(context);
	}

	private void init(AttributeSet attrs) {
		if (attrs != null && !isInEditMode()) {
			TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);
			String font = array.getString(R.styleable.FontTextView_font);
			if (font != null) {
				Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font);
				setTypeface(type);
			}
			array.recycle();
		}

		removePadding();
	}

	private void removePadding() {
		final float scale = this.getResources().getDisplayMetrics().density;
		setPadding(getPaddingLeft(), (int) (5.0f * scale), getPaddingRight(), getPaddingBottom());
	}
}
