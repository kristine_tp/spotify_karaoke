package com.yondu.base.app;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.yondu.applicationtemplate.R;
import com.yondu.base.utils.DebugLog;

public class BaseFragmentActivity extends AppCompatActivity implements OnBackStackChangedListener {

	public static final String EXTRA_FRAGMENT = "fragment";
	public static final String EXTRA_FRAGMENT_BUNDLE = "bundle";

	private Fragment mInitialFragment;

	private int mFragmentContainerId;
	private FragmentManager mFragmentManager;
	private View mLoadingView;
	private List<LifecycleObserver> lifecycleObservers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			onBaseSetContentView();
			onBaseCreate();
		} catch (Exception e) {
			e.printStackTrace();
			finish();
			startActivity(new Intent(getApplicationContext(), this.getClass()));
		}
	}

	protected void onBaseCreate() {
		setInitialFragment();
	}

	public void startActionBarActivity(Class<?> baseActionBarActivityClass, Fragment fragment) throws ClassCastException, NullPointerException {
		if (!BaseFragmentActivity.class.isAssignableFrom(baseActionBarActivityClass)) throw new ClassCastException("class must be of type BaseActionBarActivity.");

		Intent intent = new Intent(getApplicationContext(), baseActionBarActivityClass);
		intent.putExtra(BaseFragmentActivity.EXTRA_FRAGMENT, fragment.getClass().getName());
		startActivity(intent);
	}

	private void setInitialFragment() {
		if (mInitialFragment == null) {
			Bundle extras = getIntent().getExtras();
			mInitialFragment = Fragment.instantiate(getApplicationContext(), extras.getString(EXTRA_FRAGMENT), extras.getBundle(EXTRA_FRAGMENT_BUNDLE));
		}
	}

	protected void setInitialFragment(Fragment fragment) {
		mInitialFragment = fragment;
	}

	protected void onSetContentView() throws NullPointerException {
		if (findViewById(R.id.fragment_container) == null) throw new NullPointerException("Layout must contain a ViewGroup with id R.id.fragment_container");
		else mFragmentContainerId = R.id.fragment_container;
		initContentFragment();
	}

	protected FragmentManager getBaseFragmentManager() {
		return mFragmentManager;
	}

	protected void onInitActionBar() {
	}

	protected void onBackStackChanged(int backStackCount, FragmentManager.BackStackEntry topEntry) {
	}

	protected void setFirstContentFragment(Fragment fragment) {
	}

	private void initContentFragment() {
		mFragmentManager = getSupportFragmentManager();
		mFragmentManager.addOnBackStackChangedListener(this);

		try {
			setContentFragment(mInitialFragment);
		} catch (NullPointerException e) {
			DebugLog.e(this, "Initial fragment must be set. See setInitialFragment or set intent extras.");
			throw (e);
		}
	}

	private void setContentFragment(Fragment fragment) {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		transaction.add(mFragmentContainerId, fragment, fragment.getClass().getSimpleName());
		transaction.commit();
	}

	public void replaceStackedContentFragments(Fragment fragment) {
		backToFirstFragment();
		replaceContentFragment(fragment);
	}

	public void replaceContentFragment(Fragment fragment) {
		replaceContentFragment(fragment, false);
	}

	public void pushContentFragment(Fragment fragment) {
		replaceContentFragment(fragment, true);
	}

	private void replaceContentFragment(Fragment fragment, boolean isPush) {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		if (isPush) transaction.setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit, R.anim.fragment_pop_enter, R.anim.fragment_pop_exit);
		transaction.replace(mFragmentContainerId, fragment, fragment.getClass().getSimpleName());
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		transaction.addToBackStack(fragment.getClass().getSimpleName());

		if (!isPush) mFragmentManager.popBackStack();
		transaction.commit();
	}
	
	public void backToPreviousFragment() {
		 mFragmentManager.popBackStack();
	}

	public void backToFragment(Class<?> fragmentClass) {
		mFragmentManager.popBackStack(fragmentClass.getSimpleName(), 0);
	}

	public void backToFirstFragment() {
		mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}

	protected void setLoadingView(int viewResId) {
		mLoadingView = findViewById(viewResId);
		mLoadingView.setOnClickListener(null);
	}

	public void showLoading(boolean isShow) {
		if (mLoadingView != null) mLoadingView.setVisibility(isShow ? View.VISIBLE : View.GONE);
	}

	private void overridePendingTransition() {
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}

	public void registerLifecycleObserver(LifecycleObserver observer) {
		if (lifecycleObservers == null) lifecycleObservers = new ArrayList<LifecycleObserver>();
		if (!lifecycleObservers.contains(observer)) lifecycleObservers.add(observer);
	}

	public void unregisterLifecycleObserver(LifecycleObserver observer) {
		if (lifecycleObservers != null) lifecycleObservers.remove(observer);
	}

	public void unregisterLifecycleObservers() {
		if (lifecycleObservers != null) lifecycleObservers.clear();
	}

	protected void onBaseSetContentView() {
	}

	/** Layout must contain a ViewGroup with id R.id.fragment_container */
	@Override
	public void setContentView(int layoutResId) throws NullPointerException {
		super.setContentView(layoutResId);
		onSetContentView();
	}

	/** Layout must contain a ViewGroup with id R.id.fragment_container */
	@Override
	public void setContentView(View view) throws NullPointerException {
		super.setContentView(view);
		onSetContentView();
	}

	/** Layout must contain a ViewGroup with id R.id.fragment_container */
	@Override
	public void setContentView(View view, LayoutParams params) throws NullPointerException {
		super.setContentView(view, params);
		onSetContentView();
	}

	@Override
	public void onBackPressed() {
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				if(!observer.onBackPressed()) return;
			}
		}
		super.onBackPressed();
	}

	@Override
	public void onBackStackChanged() {
		FragmentManager.BackStackEntry topEntry = null;
		int backCount = mFragmentManager.getBackStackEntryCount();
		if (backCount > 0) topEntry = mFragmentManager.getBackStackEntryAt(backCount - 1);
		onBackStackChanged(backCount, topEntry);

		try {
			((BaseActivityFragment) mFragmentManager.findFragmentByTag(topEntry.getName())).asTopFragment(backCount);
		} catch (Exception e) {
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onResume();
			}
		}
		// AppEventsLogger.activateApp(this);
		overridePendingTransition();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onActivityResult(requestCode, resultCode, data);
			}
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onNewIntent(intent);
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onSaveInstanceState(outState);
			}
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onStop();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		// AppEventsLogger.deactivateApp(this);
		overridePendingTransition();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (lifecycleObservers != null) {
			for (LifecycleObserver observer : lifecycleObservers) {
				observer.onDestroy();
			}
			unregisterLifecycleObservers();
		}
	}

	public interface LifecycleObserver {

		void onResume();

		void onActivityResult(int requestCode, int resultCode, Intent data);

		void onNewIntent(Intent intent);

		void onSaveInstanceState(Bundle outState);

		void onStop();

		void onDestroy();

		boolean onBackPressed();
	}
}
