package com.yondu.base.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.yondu.applicationtemplate.R;

public class FontRadioButton extends RadioButton {

	public FontRadioButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public FontRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public FontRadioButton(Context context) {
		super(context);
	}

	private void init(AttributeSet attrs) {
		if (attrs != null && !isInEditMode()) {
			TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);
			String font = array.getString(R.styleable.FontTextView_font);
			if (font != null) {
				Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font);
				setTypeface(type);
			}
			array.recycle();
		}
	}
}
