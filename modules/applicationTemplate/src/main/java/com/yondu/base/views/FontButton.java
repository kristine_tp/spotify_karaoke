package com.yondu.base.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.yondu.applicationtemplate.R;

public class FontButton extends Button {

	public FontButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public FontButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public FontButton(Context context) {
		super(context);
	}

	private void init(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);
			String font = array.getString(R.styleable.FontTextView_font);
			if (font != null && !isInEditMode()) {
				Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font);
				setTypeface(type);
			}
			array.recycle();
		}
	}
}
