package com.yondu.base.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;

public class TabViews extends LinearLayout implements OnClickListener, OnGlobalLayoutListener {

	private int mCurrTab;
	private OnTabSwitchListener mTabListener;

	private void init() {
		getViewTreeObserver().addOnGlobalLayoutListener(this);
	}

	private void initViews() {
		for (int i = 0; i < getChildCount(); i++)
			getChildAt(i).setOnClickListener(this);
		updateTabs();
	}

	public int getCurrTab() {
		return mCurrTab;
	}
	
	public void setOnTabSwitchListener(OnTabSwitchListener listener) {
		mTabListener = listener;
		if (mTabListener != null) mTabListener.onSwitch(this, mCurrTab, false);
	}

	public void switchTab(int tabIndex) {
		boolean isSame = mCurrTab == tabIndex;
		mCurrTab = tabIndex;
		updateTabs();
		if (mTabListener != null) mTabListener.onSwitch(this, mCurrTab, isSame);
	}

	private void updateTabs() {
		for (int i = 0; i < getChildCount(); i++)
			getChildAt(i).setSelected(false);
		getChildAt(mCurrTab).setSelected(true);
	}

	@Override
	public void onClick(View view) {
		switchTab(indexOfChild(view));
	}

	public TabViews(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public TabViews(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TabViews(Context context) {
		super(context);
		init();
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public void onGlobalLayout() {
		if (getWidth() > 0) {
			initViews();
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) getViewTreeObserver().removeGlobalOnLayoutListener(this);
			else getViewTreeObserver().removeOnGlobalLayoutListener(this);
		}
	}

	public interface OnTabSwitchListener {
		void onSwitch(TabViews tabs, int tabIndex, boolean isSameTab);
	}

}