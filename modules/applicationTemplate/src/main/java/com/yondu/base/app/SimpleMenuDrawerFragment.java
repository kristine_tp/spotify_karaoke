package com.yondu.base.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yondu.applicationtemplate.R;

public class SimpleMenuDrawerFragment extends BaseActivityFragment {

	private View mMainView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mMainView = inflater.inflate(R.layout.fragment_menu_drawer, container, false);
		initView();
		return mMainView;
	}

	private void initView() {
		// mMainView.findViewById(R.id.menu_account).setOnClickListener(this);
		// mMainView.findViewById(R.id.menu_search).setOnClickListener(this);
		// mMainView.findViewById(R.id.menu_status).setOnClickListener(this);
		// mMainView.findViewById(R.id.menu_trip).setOnClickListener(this);
	}

	// private void closeDrawer() {
	// ((MenuActivity) getBaseActionBarActivity()).getMenuDrawer().closeDrawer();
	// }
	//
	// private void showFlightSearch() {
	// startActivity(new Intent(getContext(), SearchActivity.class));
	// getActivity().finish();
	// }
	//
	// @Override
	// public void onClick(View view) {
	// switch (view.getId()) {
	// case R.id.menu_search:
	// showFlightSearch();
	// break;
	// // case R.id.menu_trip:
	// // startActivity(new Intent(getContext(), BookingConfirmationActivity.class));
	// // getActivity().finish();
	// // break;
	// default:
	// closeDrawer();
	// break;
	// }
	// }
}
