package com.yondu.base.utils;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.yondu.applicationtemplate.R;

public class Connectivity {

	public static boolean isConnected(WeakReference<Activity> weakActivity) {
		return isConnected(weakActivity, false);
	}

	public static boolean isConnected(WeakReference<Activity> weakActivity, boolean isShowErrorAlert) {
		Activity activity = weakActivity.get();
		if (activity != null) {
			ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivityManager != null) {
				NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getState() == NetworkInfo.State.CONNECTED) return true;
					}
				}
			}
			if (isShowErrorAlert) showErrorAlert(activity);
		}
		return false;
	}

	private static void showErrorAlert(final Activity activity) {
		OnClickListener clicListener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) showSettings(activity);
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setTitle(R.string.alert_no_internet_title);
		builder.setMessage(R.string.alert_no_internet_msg);
		builder.setPositiveButton(R.string.settings, clicListener);
		builder.setNegativeButton(R.string.cancel, clicListener);
		builder.create().show();
	}

	private static void showSettings(Activity activity) {
		ComponentName component = new ComponentName("com.android.settings", "com.android.settings.Settings");
		Intent intent = new Intent(Intent.ACTION_MAIN, null);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setComponent(component);
		activity.startActivity(intent);
	}
}
