package com.yondu.base.utils;

import java.text.NumberFormat;

import android.annotation.SuppressLint;

public class FormatUtils {

	@SuppressLint("DefaultLocale")
	public static String capFirstChar(String text) {
		String capText = text;
		if (text != null && text.length() > 0) {
			capText = String.valueOf(text.charAt(0)).toUpperCase();
			if (text.length() > 1) capText += text.substring(1, text.length());
		}
		return capText;
	}

	public static String padZeros(int num, int minDigits) {
		String numString = String.valueOf(num);
		for (int i = numString.length(); i < minDigits; i++) {
			numString = "0" + numString;
		}
		return numString;
	}
	
	public static String formatCurrency(float num) {
		return NumberFormat.getCurrencyInstance().format(num).substring(1);
	}
	
	public static String getTextSinglePluralForm(int num, String singular, String plural, boolean appendNum) {
		return (appendNum ? num + " " : "") + (num == 1 ? singular : plural);
	}

}
