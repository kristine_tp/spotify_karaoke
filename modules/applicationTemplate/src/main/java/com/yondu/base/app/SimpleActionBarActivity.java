package com.yondu.base.app;

import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.FragmentManager;

import com.yondu.applicationtemplate.R;
import com.yondu.base.app.BaseActionBar.ActionHome;
import com.yondu.base.utils.DebugLog;

public class SimpleActionBarActivity extends BaseActionBarActivity {

	private int mHomeImgResId, mBackImgResId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onBaseSetContentView() {
		setContentView(R.layout.activity_base);
		setLoadingView(R.id.main_loading);
	}

	public void setActionBarNavButtons(int homeImgResId, int backImgResId) {
		mHomeImgResId = homeImgResId;
		mBackImgResId = backImgResId;
		setActionHome(ActionHome.HOME);
	}

	public void setActionHome(ActionHome homeAction) {
		try {
			getBaseActionBar().setHomeAction(homeAction, homeAction == ActionHome.HOME ? mHomeImgResId : mBackImgResId);
		} catch(NullPointerException e) {
			DebugLog.eStackTrace(e);
		}
	}

	@Override
	protected void onBackStackChanged(int backStackCount, FragmentManager.BackStackEntry topEntry) {
		super.onBackStackChanged(backStackCount, topEntry);
		setActionHome(backStackCount <= 0 ? ActionHome.HOME : ActionHome.BACK);
	}

}
